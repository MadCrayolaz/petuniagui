=======
Read Me
=======
To use the launcher, follow these 5 steps:
1. Input the path of your ARMA II Operation Arrowhead folder.  If you installed it to the default location, use the default location button.
2. Input the path of your ARMA II folder.  If you installed it to the default location, use the default location button.
3. Select the number of cores your CPU has.
4. Press done.
5. Select the server you wish to join.

If you made a mistake during the configuration file generating step, press the "Delete Config File" button, and restart the program.

If you are having issues joining a server, delete your cache using the "Delete Cache" button.

If you have any other issues, email the developer at dynstyw@gmail.com or message the mods on the forums to get in contact with the developer