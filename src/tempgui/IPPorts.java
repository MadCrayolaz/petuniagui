package tempgui;

public class IPPorts {

    String cip;
    String cport;
    String nip;
    String nport;
    String pip;
    String pport;

    public IPPorts(String cip, String cport, String nip, String nport, String pip, String pport) {
        this.cip = cip;
        this.cport = cport;
        this.nip = nip;
        this.nport = nport;
        this.pip = pip;
        this.pport = pport;
    }
}
