package tempgui;

public class Configuration {

    String cores;
    String OAPath;
    String AIIPath;

    public Configuration(String cores, String OAPath, String AIIPath) {
        this.cores = cores;
        this.OAPath = OAPath;
        this.AIIPath = AIIPath;
    }
}