package tempgui;

import com.google.gson.Gson;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SetupLayout extends javax.swing.JFrame {

    private ConfigurationFinishedListener listener;
    private File configFile = null;
    private File configDir = null;

    private static String myDocuments;

    public interface ConfigurationFinishedListener {

        void OnConfigurationFinished();
    }

    public SetupLayout(ConfigurationFinishedListener listener) {
        this.listener = listener;
        getMyDocs();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DirChooser = new javax.swing.JFileChooser();
        OADef = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        OALoc = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        FinishButton = new javax.swing.JButton();
        AIIDef = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        AIILoc = new javax.swing.JTextField();
        CoreSpinner = new javax.swing.JSpinner();

        DirChooser.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
        DirChooser.setApproveButtonText("Select");
        DirChooser.setCurrentDirectory(new java.io.File("C:\\"));
            DirChooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
            DirChooser.setForeground(java.awt.Color.white);

            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

            OADef.setText("Default Path - Operation Arrowhead Folder");
            OADef.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    OADefActionPerformed(evt);
                }
            });

            jButton2.setText("...");
            jButton2.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton2ActionPerformed(evt);
                }
            });

            OALoc.setText("Operation Arrowhead Location");
            OALoc.setName("OALoc"); // NOI18N
            OALoc.addFocusListener(new java.awt.event.FocusAdapter() {
                public void focusGained(java.awt.event.FocusEvent evt) {
                    OALocFocusGained(evt);
                }
                public void focusLost(java.awt.event.FocusEvent evt) {
                    OALocFocusLost(evt);
                }
            });

            jLabel1.setText("# of Cores");

            FinishButton.setText("Done");
            FinishButton.setEnabled(false);
            FinishButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    FinishButtonActionPerformed(evt);
                }
            });

            AIIDef.setText("Default Path - ARMA II Folder");
            AIIDef.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    AIIDefActionPerformed(evt);
                }
            });

            jButton4.setText("...");
            jButton4.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton4ActionPerformed(evt);
                }
            });

            AIILoc.setText("ARMA II Location");
            AIILoc.addFocusListener(new java.awt.event.FocusAdapter() {
                public void focusGained(java.awt.event.FocusEvent evt) {
                    AIILocFocusGained(evt);
                }
                public void focusLost(java.awt.event.FocusEvent evt) {
                    AIILocFocusLost(evt);
                }
            });

            CoreSpinner.setModel(new javax.swing.SpinnerListModel(new String[] {"1", "2", "4", "8", "16"}));

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(OADef, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(CoreSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(FinishButton))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(AIILoc))
                        .addComponent(AIIDef, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(OALoc)))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(OADef)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(OALoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(AIIDef)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton4)
                        .addComponent(AIILoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(FinishButton)
                        .addComponent(CoreSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void OALocFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_OALocFocusGained
        if(this.OALoc.getText().equals("Operation Arrowhead Location")) {
            this.OALoc.setText("");
        }
    }//GEN-LAST:event_OALocFocusGained

    private void OALocFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_OALocFocusLost
        if(this.OALoc.getText().equals("")) {
            this.OALoc.setText("Operation Arrowhead Location");
        }
    }//GEN-LAST:event_OALocFocusLost

    private void AIIDefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AIIDefActionPerformed
        this.AIILoc.setText("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Arma 2");
        if(!(OALoc.getText().equals("Operation Arrowhead Location")) && !(OALoc.getText().equals("")) && !(AIILoc.getText().equals("ARMA II Location")) && !(AIILoc.getText().equals(""))) {
            FinishButton.setEnabled(true);
        }
    }//GEN-LAST:event_AIIDefActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        this.DirChooser.showDialog(this, "Select");
        File s = this.DirChooser.getSelectedFile();
        this.AIILoc.setText(s.toString());
        if(!(OALoc.getText().equals("Operation Arrowhead Location")) && !(OALoc.getText().equals("")) && !(AIILoc.getText().equals("ARMA II Location")) && !(AIILoc.getText().equals(""))) {
            FinishButton.setEnabled(true);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void AIILocFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_AIILocFocusGained
        if(this.AIILoc.getText().equals("ARMA II Location")) {
            this.AIILoc.setText("");
        }
    }//GEN-LAST:event_AIILocFocusGained

    private void AIILocFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_AIILocFocusLost
        if(this.AIILoc.getText().equals("")) {
            this.AIILoc.setText("ARMA II Location");
        }
    }//GEN-LAST:event_AIILocFocusLost

    private void FinishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FinishButtonActionPerformed
        configDir = new File(myDocuments + "\\PetuniaGUI\\");
        configFile = new File(myDocuments + "\\PetuniaGUI\\config.txt");
        Configuration config = new Configuration(getCores(), getOAPath(), getAIIPath());
        Gson gson = new Gson();
        configDir.mkdirs();
        try {
            FileWriter fw = new FileWriter(configFile);
            fw.write(gson.toJson(config));
            fw.close();
        }
        catch(IOException ex) {
            Logger.getLogger(SetupLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(listener != null) {
            listener.OnConfigurationFinished();
        }
    }//GEN-LAST:event_FinishButtonActionPerformed

    private void OADefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OADefActionPerformed
        this.OALoc.setText("C:\\Program Files (x86)\\Steam\\steamapps\\common\\arma 2 operation arrowhead");
        if(!(OALoc.getText().equals("Operation Arrowhead Location")) && !(OALoc.getText().equals("")) && !(AIILoc.getText().equals("ARMA II Location")) && !(AIILoc.getText().equals(""))) {
            FinishButton.setEnabled(true);
        }
    }//GEN-LAST:event_OADefActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.DirChooser.showDialog(this, "Select");
        File s = this.DirChooser.getSelectedFile();
        this.OALoc.setText(s.toString());
        if(!(OALoc.getText().equals("Operation Arrowhead Location")) && !(OALoc.getText().equals("")) && !(AIILoc.getText().equals("ARMA II Location")) && !(AIILoc.getText().equals(""))) {
            FinishButton.setEnabled(true);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    public String getOAPath() {
        return OALoc.getText();
    }

    public String getAIIPath() {
        return AIILoc.getText();
    }

    public String getCores() {
        final String numOfCores = (String) CoreSpinner.getModel().getValue();
        return numOfCores;
    }

    public void getMyDocs() {
        try {
            Process p = Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
            p.waitFor();

            InputStream in = p.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();

            myDocuments = new String(b);
            myDocuments = myDocuments.split("\\s\\s+")[4];

        }
        catch(Throwable t) {
            t.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AIIDef;
    private javax.swing.JTextField AIILoc;
    private javax.swing.JSpinner CoreSpinner;
    private javax.swing.JFileChooser DirChooser;
    private javax.swing.JButton FinishButton;
    private javax.swing.JButton OADef;
    private javax.swing.JTextField OALoc;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
