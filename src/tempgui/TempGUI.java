package tempgui;

import com.google.gson.Gson;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TempGUI extends javax.swing.JFrame {

    private Gson gson = new Gson();
    private Configuration config;
    private IPPorts ipports;
    private File configF = null;
    private File cache1 = new File(System.getenv("USERPROFILE") + "\\AppData\\Local\\ArmA 2 OA\\MPMissionsCache");
    private File cache2 = new File(System.getenv("USERPROFILE") + "\\AppData\\Local\\ArmA 2 OA\\BattlEye");
    private File cache3 = new File(System.getenv("USERPROFILE") + "\\AppData\\Local\\ArmA 2 OA");
    private static String myDocuments;
    
    public TempGUI() {
        initComponents();
        getMyDocs();
        try {
            config = gson.fromJson(readFile(myDocuments + "\\PetuniaGUI\\config.txt", Charset.defaultCharset()), Configuration.class);
            ipports = gson.fromJson(readFile(myDocuments + "\\PetuniaGUI\\ipports.txt", Charset.defaultCharset()), IPPorts.class);
        }
        catch(IOException ex) {
            Logger.getLogger(SetupLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chernarusBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        namalskBtn = new javax.swing.JButton();
        napfBtn = new javax.swing.JButton();
        ResetButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        DeleteCache = new javax.swing.JButton();
        HelpButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 102));
        setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        chernarusBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        chernarusBtn.setText("Join!");
        chernarusBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        chernarusBtn.setMaximumSize(new java.awt.Dimension(23, 103));
        chernarusBtn.setMinimumSize(new java.awt.Dimension(1, 1));
        chernarusBtn.setPreferredSize(new java.awt.Dimension(23, 103));
        chernarusBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chernarusBtnMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("TI Uni", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Chernarus");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel2.setFont(new java.awt.Font("TI Uni", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Namalsk");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel3.setFont(new java.awt.Font("TI Uni", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("NAPF");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        namalskBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        namalskBtn.setText("Join!");
        namalskBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        namalskBtn.setMaximumSize(new java.awt.Dimension(23, 103));
        namalskBtn.setMinimumSize(new java.awt.Dimension(1, 1));
        namalskBtn.setPreferredSize(new java.awt.Dimension(23, 103));
        namalskBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                namalskBtnMouseClicked(evt);
            }
        });

        napfBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        napfBtn.setText("Join!");
        napfBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        napfBtn.setMaximumSize(new java.awt.Dimension(23, 103));
        napfBtn.setMinimumSize(new java.awt.Dimension(1, 1));
        napfBtn.setPreferredSize(new java.awt.Dimension(23, 103));
        napfBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                napfBtnMouseClicked(evt);
            }
        });

        ResetButton.setText("Delete Config File");
        ResetButton.setToolTipText("");
        ResetButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        ResetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetButtonActionPerformed(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("To Generate New Config, Restart Launcher");
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jSeparator2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        DeleteCache.setText("Delete Cache");
        DeleteCache.setToolTipText("");
        DeleteCache.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        DeleteCache.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteCacheActionPerformed(evt);
            }
        });

        HelpButton.setText("Having Issues? Click Me!");
        HelpButton.setToolTipText("");
        HelpButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        HelpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HelpButtonActionPerformed(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tempgui/logo-small.jpg"))); // NOI18N
        jLabel6.setToolTipText("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chernarusBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(namalskBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(napfBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ResetButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                    .addComponent(jSeparator2)
                    .addComponent(DeleteCache, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(HelpButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chernarusBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(namalskBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(napfBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(ResetButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(DeleteCache)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(HelpButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private void chernarusBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chernarusBtnMouseClicked
        String[] cmd = {config.OAPath + "\\ARMA2OA_BE.exe", "0", "0", "-mod=" + config.AIIPath + ";Expansion;@DayZOverwatch;@DayZ_Epoch;", "-connect=" + ipports.cip, "-port=" + ipports.cport, "-nosplash", "-noPause", "-nologs", "-skipintro", "-cpuCount=" + config.cores};
        try {
            Runtime.getRuntime().exec(cmd);
        }
        catch(IOException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_chernarusBtnMouseClicked

    private void namalskBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_namalskBtnMouseClicked
        String[] cmd = {config.OAPath + "\\ARMA2OA_BE.exe", "0", "0", "-mod=" + config.AIIPath + ";Expansion;@DayZOverwatch;@DayZ_Namalsk;@DayZ_Epoch;", "-connect=" + ipports.nip, "-port=" + ipports.nport, "-nosplash", "-noPause", "-nologs", "-skipintro", "-cpuCount=" + config.cores};
        try {
            Runtime.getRuntime().exec(cmd);
        }
        catch(IOException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_namalskBtnMouseClicked

    private void napfBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_napfBtnMouseClicked
        String[] cmd = {config.OAPath + "\\ARMA2OA_BE.exe", "0", "0", "-mod=" + config.AIIPath + ";Expansion;@DayZOverwatch;@DayZ_Epoch;", "-connect=" + ipports.pip, "-port=" + ipports.pport, "-nosplash", "-noPause", "-nologs", "-skipintro", "-cpuCount=" + config.cores};
        try {
            Runtime.getRuntime().exec(cmd);
        }
        catch(IOException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_napfBtnMouseClicked

    private void ResetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetButtonActionPerformed
        configF = new File(myDocuments + "\\PetuniaGUI\\config.txt");
        configF.delete();
    }//GEN-LAST:event_ResetButtonActionPerformed

    private void DeleteCacheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteCacheActionPerformed
        try {
            removeRecursive(Paths.get(cache1.getAbsolutePath()));
            removeRecursive(Paths.get(cache2.getAbsolutePath()));
            removeRecursive(Paths.get(cache3.getAbsolutePath()));
        }
        catch(IOException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_DeleteCacheActionPerformed

    private void HelpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HelpButtonActionPerformed
        try {
            URL url = new URL("http://www.petuniaserver.com/launcherhelp/");
            URI uri = url.toURI();
            java.awt.Desktop.getDesktop().browse(uri);
        }
        catch(MalformedURLException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(IOException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(URISyntaxException ex) {
            Logger.getLogger(TempGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_HelpButtonActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch(ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TempGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(InstantiationException ex) {
            java.util.logging.Logger.getLogger(TempGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TempGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch(javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TempGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TempGUI().setVisible(true);
            }
        });
    }

    public static void removeRecursive(Path path) throws IOException {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                // try to delete the file anyway, even if its attributes
                // could not be read, since delete-only access is
                // theoretically possible
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if(exc == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
                else {
                    // directory iteration failed; propagate exception
                    throw exc;
                }
            }
        });
    }
    
    public void getMyDocs() {
        try {
            Process p = Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
            p.waitFor();

            InputStream in = p.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();

            myDocuments = new String(b);
            myDocuments = myDocuments.split("\\s\\s+")[4];

        }
        catch(Throwable t) {
            t.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton DeleteCache;
    private javax.swing.JButton HelpButton;
    private javax.swing.JButton ResetButton;
    private javax.swing.JButton chernarusBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JButton namalskBtn;
    private javax.swing.JButton napfBtn;
    // End of variables declaration//GEN-END:variables
}
