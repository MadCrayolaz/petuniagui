package tempgui;

import java.awt.EventQueue;
import java.io.*;
import tempgui.SetupLayout.ConfigurationFinishedListener;

public class PetuniaMain implements ConfigurationFinishedListener {

    private SetupLayout confWindow;
    private TempGUI tempGUI;
    private File configFile = new File(System.getenv("USERPROFILE") + "\\My Documents\\PetuniaGUI\\config.txt");
    private static String myDocuments;
    
    private PetuniaMain() {
        getMyDocs();
        init();
    }

    private void init() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    tempGUI = new TempGUI();
                    confWindow = new SetupLayout(PetuniaMain.this);
                    if(!configFilePresent()) {
                        confWindow.setVisible(true);
                    }
                    else {
                        OnConfigurationFinished();
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    private boolean configFilePresent() {
        if(configFile.exists()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        new PetuniaMain();
    }

    @Override
    public void OnConfigurationFinished() {
        confWindow.setVisible(false);
        tempGUI.setVisible(true);
    }
     
    public void getMyDocs() {
        try {
            Process p = Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
            p.waitFor();

            InputStream in = p.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();

            myDocuments = new String(b);
            myDocuments = myDocuments.split("\\s\\s+")[4];

        }
        catch(Throwable t) {
            t.printStackTrace();
        }
    }
}
